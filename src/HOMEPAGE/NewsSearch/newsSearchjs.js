var baseURL = 'https://newsapi.org/v2/top-headlines';
var apiKey = "5f35d358474042339379c6818aadaa35";
const countries = [ 
  {
    'name' : 'India',
    'code' : 'in'
  },
  {
    'name' : 'USA',
    'code' : 'us'
  },
  {
    'name' : 'Egypt',
    'code' : 'eg'
  }
  ]
  const categories =
  ['business', 'entertainment', 'technology', 'general']

    getSelectCountry();
    getSelectCategory();
    getNews();

function getSelectCountry(){
  const countryId = document.querySelector("#countryId");

  let selectCountry = `<select class="form-control" id="selectedCountry">`;
  countries.forEach( country => {
    selectCountry += `<option value=${country['code']}>${country['name']}</option>`
  });

  selectCountry += `</select>`;
  console.log(selectCountry);
  countryId.innerHTML = selectCountry;
  
}

function getSelectCategory(){
  
  const categoryId = document.querySelector("#categoryId");

  let selectCategory = `<select class="form-control" id="selectedCategory">`;
  categories.forEach(category =>{
    selectCategory += `<option value=${category}>${category}</option>`
  });

  selectCategory += `</select>`;
  categoryId.innerHTML = selectCategory;
 
}

function getNews(){
  debugger;
  const country = document.querySelector("#selectedCountry").value; 
  const category = document.querySelector("#selectedCategory").value;
  const searchNews = document.getElementById("searchId").value;
  console.log(searchNews);
 // console.log(country);
  //console.log(category);
  let url = `${baseURL}?country=${country}&category=${category}&q=${searchNews}&apiKey=${apiKey}`; 
    let news = [];
    fetch(url).then(res => res.json()).then(   
        result => {
          // console.log(result);
            news = result['articles'];
            viewNews(news);
          //console.log(news);
        });     
}

function viewNews(news){
    let newItemId = document.getElementById("newsItemId");
   let data='';
     news.forEach(element => {
        // console.log(element);
         data += `<div class="col-md-3">
         <div class="card" style="width: 17rem;">
       <img src="${element['urlToImage']}" class="card-img-top" alt="...">
         <div class = "card-body">
        <h5 class="card-title">${element['title']}</h5>
        <p class = "card-text">${element['description']}</p>
        <a href = "${element['url']}" class="btn btn-primary">Go Somewhere</a>
        </div>
        </div>
        </div>`
     });
     newItemId.innerHTML = data;
}

