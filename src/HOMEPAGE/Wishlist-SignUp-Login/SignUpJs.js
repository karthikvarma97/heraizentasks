function addUsers() {
    let fullName = document.querySelector("#name").value;
    let doB = document.querySelector("#doB").value;
    let email = document.querySelector("#email").value;
    let password = document.querySelector("#password").value;
    console.log(fullName);

    //debugger;
    let data = localStorage.getItem('details') ? JSON.parse(localStorage.getItem('details')) : [];

    var usersData = { fullName: fullName, doB: doB, email: email, password: password };

    data.push(usersData);

    localStorage.setItem("details", JSON.stringify(data));
    resetForm();
    //alert("Yor have successfully created account..!!")
    window.location.href = "index.html"
}

//Check is the given password is correct with the repassword
let vPwd = false;
function verifyPassword(input) {
    if (input.value != document.getElementById("password").value) {
        // alert("Password Must be Matching");
        document.getElementById("p4").innerHTML = "Password Must be Matching.";
        document.getElementById("confirmPwd").style.borderColor = "#FF0000";
    }
    else {
        vPwd = true;
        document.getElementById("p4").innerHTML = "";
        document.getElementById("confirmPwd").style.borderColor = "#008000";
    }
}

//Reset the data in the form
function resetForm() {
    document.getElementById("name").value = "";
    document.getElementById("doB").value = "";
    document.getElementById("email").value = "";
    document.getElementById("password").value = "";
    document.getElementById("confirmPwd").value = "";
}

//Check the input name is correct
let nme = false;
function verifyName(inputtxt) {
    //debugger;
    var field = inputtxt.value;
    var str = field.split(" ").join("");
    console.log(str);
    var letters = /^[0-9a-zA-Z]+$/;
    if (field.length >= 4 && str.match(letters)) {
        // alert('Your name have accepted : you can try another');
        nme = true;
        document.getElementById("name").style.borderColor = "#008000";
        document.getElementById("p2").innerHTML = "";
    }
    else {
        //alert('Please enter Valid Name');
        document.getElementById("p2").innerHTML = "Please enter Valid Name.";
        nme = false;
        document.getElementById("name").style.borderColor = "#FF0000";
    }
}

//check the mail id is correct
let eml = false;
function ValidateEmail(inputText) {
    // alert("mail");
    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (inputText.value.match(mailformat)) {
        eml = true;
        document.getElementById("email").style.borderColor = "#008000";
        document.getElementById("p1").innerHTML = "";
    }
    else {
        //alert("You have entered an invalid email address!");
        document.getElementById("p1").innerHTML = "You have entered an invalid email address!";
        eml = false;
        document.getElementById("email").style.borderColor = "#FF0000";
    }
}

//Check the password is in correct format
let pwd = false;
function CheckPassword(inputtxt) {
    var paswd = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{7,15}$/;
    if (inputtxt.value.match(paswd)) {
        //alert('Correct');
        pwd = true;
        document.getElementById("password").style.borderColor = "#008000";
        document.getElementById("p3").innerHTML = "";
    }
    else {
        //alert('Try another...!')
        document.getElementById("password").style.borderColor = "#FF0000";
        document.getElementById("p3").innerHTML = "Try another...!";
        pwd = false;
        
    }
}
//Compare the give date
dte = false;
function CompareDate(indate) {
    if(indate == null){
        document.getElementById("doB").style.borderColor = "#FF0000";
    }
    else{
        document.getElementById("doB").style.borderColor = "#008000";
        dte = true;
    }
}

function checkAlldata() {
    //debugger;
    // alert("check");
    if (pwd == true && nme == true && eml == true && vPwd == true) {
        var inputText = document.getElementById("email").value;
        console.log(inputText);
        emailExist(inputText);
        if (emEx == true) {
            addUsers();
        }
    }
    else {
        resetColour();
        //alert("Enter the Valid Fields");
        
    }
}

//Email validation to check is there same email after the submit
emEx = false;
function emailExist(value) {
    //alert(email);
    let currentEmail = value;
    let existemail = JSON.parse(localStorage.getItem("details"));
    if (existemail != null) {
        var length = Object.keys(existemail).length;
        console.log(existemail);
        for (let i = 0; i < length; i++) {
            if (currentEmail == existemail[i].email) {
                // console.log(existemail[i].email);
                document.getElementById("email").style.borderColor = "#FF0000";
                document.getElementById("p1").innerHTML = "Email is already registered..!!";
                //alert("Email is already registered..!!");
                return emEx = false;
            }
        }
        emEx = true;
        document.getElementById("p1").innerHTML = "";
        document.getElementById("email").style.borderColor = "#008000";
    }
    else {
        emEx = true;
        document.getElementById("email").style.borderColor = "#008000";
    }

}
//Register Pressed Without data
function resetColour() {
    if(nme == true){
        document.getElementById("name").style.borderColor = "#008000";
    }
    else{
        document.getElementById("name").style.borderColor = "#FF0000";
    }

    if(pwd == true){
        document.getElementById("password").style.borderColor = "#008000";
    }
    else{
        document.getElementById("password").style.borderColor = "#FF0000";
    }

    if(eml == true){
        document.getElementById("email").style.borderColor = "#008000";
    }
    else{
        document.getElementById("email").style.borderColor = "#FF0000";
    }

    if(dte == true){
        document.getElementById("doB").style.borderColor = "#008000";
    }
    else{
        document.getElementById("doB").style.borderColor = "#FF0000";
    }
    
    if(vPwd == true){
        document.getElementById("confirmPwd").style.borderColor = "#008000";
    }
    else{
        document.getElementById("confirmPwd").style.borderColor = "#FF0000";
    }
}
